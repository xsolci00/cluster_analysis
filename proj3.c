/**
 * Kostra programu pro 3. projekt IZP 2015/16
 *
 * Jednoducha shlukova analyza: 2D nejblizsi soused.
 * Single linkage
 * http://is.muni.cz/th/172767/fi_b/5739129/web/web/slsrov.html
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX
#include <string.h>
#include <ctype.h>
#include <errno.h>

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *      #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - "__FILE__":%u: "s"\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *      pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */

struct obj_t {
    int id;
    float x;
    float y;
};

struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/*
 Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 Ukazatel NULL u pole objektu znamena kapacitu 0.
*/
void init_cluster(struct cluster_t *c, int cap)
{
    assert(c != NULL);
    assert(cap >= 0);

    if(cap == 0)
    	c->obj = NULL;
    //pokud neni prazdni shluk, rozsiri shluk mallocem o 'cap' objektů
    else{
    	c->obj = malloc(sizeof(struct obj_t)* cap);
        if(c->obj == NULL)
        	fprintf(stderr, "Error: allocation failed!\n");
        else
        	c->capacity = cap;

    }
    c->size = 1;
}

/*
 Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 */
void clear_cluster(struct cluster_t *c)
{

	free(c->obj);

	c->obj = NULL;
	init_cluster(c, 0);
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/*
 Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
    // TUTO FUNKCI NEMENTE
    assert(c);
    assert(c->capacity >= 0);
    assert(new_cap >= 0);

    if (c->capacity >= new_cap)
        return c;

    size_t size = sizeof(struct obj_t) * new_cap;

    void *arr = realloc(c->obj, size);
    if (arr == NULL)
        return NULL;

    c->obj = arr;
    c->capacity = new_cap;
    return c;
}

/*
 Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 nevejde.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
    //pokud se objekt vejde prida ho na konec
	if((c->size ) < c->capacity)
    	c->obj[c->size ] = obj;
    //jinak rozsiri shluk o 'CLUSTER_CHUNK'
	else{
    	c = resize_cluster(c, c->capacity + CLUSTER_CHUNK);
    	c->obj[c->size ] = obj;
    }

    c->size = c->size + 1;
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);

/*
 Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 Objekty ve shluku 'c1' budou serazny vzestupne podle identifikacniho cisla.
 Shluk 'c2' bude nezmenen.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c2 != NULL);
    int i = 0;
    int id_eqv = 0;

    //prochazi ve whilu celou delku shluku c2 a pokud nenajde shodu ID
    //se shlukem c1 je pridan na konec shluku c1
    while(i < c2->size){
    	for(int x = 0; x < c1->size; x++){
    		if(c1->obj[x].id == c2->obj[i].id)
    			id_eqv = 1;
    	}
    	if(id_eqv != 1)
    		append_cluster(c1, c2->obj[i]);
    	i++;
    }
    //volani funkce na serazeni clusteru c1
    sort_cluster(&(c1[0]));
}

/**********************************************************************/
/* Prace s polem shluku */

/*
 Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 pocet shluku v poli.
*/
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
    assert(idx < narr);
    assert(narr > 0);

    //vycisti cluster
    clear_cluster(&(carr[idx]));

    //zbytek clusteru posunut o 1
    memmove(&(carr[idx]),&(carr[idx+1]),(sizeof(struct cluster_t)*(narr - idx)));

    //zmena velikosti clusteru
    narr --;
    return narr;
}

/*
 Pocita Euklidovskou vzdalenost mezi dvema objekty.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
    assert(o1 != NULL);
    assert(o2 != NULL);

    double dis_x, dis_y, distance;

    //vypocet rozdilu na x a y ose
    dis_x = (o1->x) - (o2->x);
    dis_y = (o1->y) - (o2->y);

    //druha odmocnina z ((dis_x)^2 + (dis_y)^2)
    distance = sqrtf((dis_x * dis_x) + (dis_y * dis_y));



    return distance;
}

/*
 Pocita vzdalenost dvou shluku. Vzdalenost je vypoctena na zaklade nejblizsiho
 souseda.
*/
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c1->size > 0);
    assert(c2 != NULL);
    assert(c2->size > 0);

    //x <= 1000 2 <= 1000, max distance = 1414.21...
    //proto inicializace distance = 1500
    double distance = 1500;
    double new_distance;

    //projde vzdalenost kazdeho clusteru s kazdym a pamatuje si nejmensi
    //vysledek id sluterů do 'c1' a 'c2'
    int x = 0;
    for(int i = 0; i < c1->size; i++){
    	for(x = 0; x < c2->size; x++){
    		new_distance = obj_distance(&c1->obj[i], &c2->obj[x]);
    		if(new_distance < distance)
    			distance = new_distance;

    	}
    }

    return distance;
}

/*
 Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 hleda dva nejblizsi shluky (podle nejblizsiho souseda). Nalezene shluky
 identifikuje jejich indexy v poli 'carr'. Funkce nalezene shluky (indexy do
 pole 'carr') uklada do pameti na adresu 'c1' resp. 'c2'.
*/
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
    assert(narr > 0);
    //x <= 1000 2 <= 1000, max distance = 1414.21...
    //proto inicializace distance = 1500
    double distance = 1500;
    double new_distance;

    //projde vzdalenost kazdeho clusteru s kazdym a pamatuje si nejmensi
    //vysledek id sluterů do 'c1' a 'c2'
    for(int x = 0; x < narr; x++){
    	for(int y = 0; y < narr; y++){
    		new_distance = cluster_distance(&carr[x], &carr[y]);
    		if(new_distance <= distance && x != y){
    			distance = new_distance;
    			*c1 = x;
    			*c2 = y;
    		}
    	}
    }
}

// pomocna funkce pro razeni shluku
static int obj_sort_compar(const void *a, const void *b)
{
    // TUTO FUNKCI NEMENTE
    const struct obj_t *o1 = a;
    const struct obj_t *o2 = b;
    if (o1->id < o2->id) return -1;
    if (o1->id > o2->id) return 1;
    return 0;
}

/*
 Razeni objektu ve shluku vzestupne podle jejich identifikatoru.
*/
void sort_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/*
 Tisk shluku 'c' na stdout.
*/
void print_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    for (int i = 0; i < c->size; i++)
    {
        if (i) putchar(' ');
        printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
    }
    putchar('\n');
}

/*
 Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
*/
int load_clusters(char *filename, struct cluster_t **arr)
{
    assert(arr != NULL);
    FILE * pFile;
    char * buff;
    int count = 0;
    int objid;
    double x;
    double y;

    //buffer pro nacteni poctu 'count=xxxx'
    buff = malloc(sizeof(char) * 14);
    if(buff == NULL){
    	fprintf(stderr, "Error: allocation failed!\n");
    	*arr = NULL;
    	return 0;
    }

    //otevreni souboru
    pFile = fopen(filename, "r");
    if(pFile == NULL){
    	*arr = NULL;
    	return 0;
    }
    //nacteni do bufferu
    fgets(buff, 14, pFile);
    int i = 0;
    //hledani zacatku cisla
    while(isdigit(buff[i]) == 0)
    	i++;
    //ukazatel na cislo 'count' a prevod na integer
    char * ptr_count = &buff[i];
    long int cluster_count = strtol(ptr_count, NULL, 10);

    if (errno != 0){			//dotaz na chybu pri prevodu argumentu na double
        int error_num = errno;	//vypis cisla a popisu chyby
        fprintf(stderr, "Error number %d: %s \n", error_num, strerror(error_num));
    	*arr = NULL;
        return 0;
    }

    //alokace pole clusteru
    *arr = malloc(sizeof(struct cluster_t) * cluster_count);
    //ukladani objektu do clusteru po jednom
    for(count = 0; count  < cluster_count; count++){
    	int fscan_stat = fscanf(pFile, "%d" "%lf" "%lf", &objid, &x, &y);
       	if(x > 1000 || y > 1000)
       		break;
    	if(fscan_stat != 3)
       			break;
    	init_cluster(&(*arr)[count], 1);
       	(*arr)[count].obj->id = objid;
    	(*arr)[count].obj->x = x;
    	(*arr)[count].obj->y = y;
   }

    //uzavreni souboru uvolneni bufferu
    fclose(pFile);
    free(buff);

   	if(count != cluster_count){
   		fprintf(stderr,"Error while reading imput file\n");
    	*arr = NULL;
        return 0;
   	}

    return cluster_count;
}

/*
 Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 Tiskne se prvnich 'narr' shluku.
*/
void print_clusters(struct cluster_t *carr, int narr)
{
    printf("Clusters:\n");
    for (int i = 0; i < narr; i++)
    {
        printf("cluster %d: ", i);
        print_cluster(&carr[i]);
    }
}

int main(int argc, char *argv[])
{
    struct cluster_t *clusters = NULL;
    int cluster_request = 1;
    int *a;
    int *b;
    a = malloc(sizeof(int));
    b = malloc(sizeof(int));
    if(a == NULL || b == NULL)
    	fprintf(stderr, "Error: allocation failed!\n");

    if(argc > 3 || argc < 2){
    	fprintf(stderr,"Error: Invalid arguments\n");
    	return -1;
    }

    //volani load_clusters, nacteni clusteru ze souboru
    //a ziskani pocku clusterů 'cluster_count'
    int cluster_count = load_clusters(argv[1],&clusters);
    if(clusters == NULL){
    	fprintf(stderr, "Error : Can't load any clusters\n");
    	return -1;
    }

    //nacte pozadovany pocet shluku jako argumet spusteni

    if(argc == 3)
    	cluster_request = strtod(argv[2], NULL);
    if(cluster_request < 1)
    	return 0;
    if (errno != 0){			//dotaz na chybu pri prevodu argumentu na double
    	int error_num = errno;	//vypis cisla a popisu chyby
    	fprintf(stderr, "Error number %d: %s \n", error_num, strerror(error_num));
    	return -1;
    }

    if(cluster_request > cluster_count){
    	fprintf(stderr,"Error: more clusters than objects \n");
    	return -1;
    }

    //do dosazeni pozadovaneho poctu shluků hleda nejblizsi
    //a spojuje do jednoho (merge+remove)
    while(cluster_count > cluster_request){
    	find_neighbours(&(*clusters), cluster_count, a, b);
		merge_clusters(&(clusters[*a]),&(clusters[*b]));
		cluster_count = remove_cluster(&(*clusters), cluster_count, *b);
    }

    //seradi objekty v shlucich
    //nicmene je radi i ve funkci merge_cluusters
    for(int i = 0; i < cluster_count; i++)
    	sort_cluster(&(clusters[i]));

    //tisk shluků
    print_clusters(&(*clusters), cluster_count);

    //uvolneni pameti
    while(cluster_count > 0)
    	cluster_count = remove_cluster(&(*clusters), cluster_count,(cluster_count - 1));
    free(clusters);
    free(a);
    free(b);

//prototyp pouziti funkce:
//	    find_neighbours(&(arr[0]), cluster_count, a, b);
//		merge_clusters(&(arr[1]),&(arr[0]));
//   	double cluster_dis = cluster_distance(&(arr[0]),&(arr[1]));
// 	 	double distance = obj_distance(&arr[0].obj[1], &arr[0].obj[0]);
//   	append_cluster(&(arr[0]), *(arr[1].obj));
//		cluster_count = remove_cluster(&(*arr), cluster_count, 5);
	return 0;
}
